import { useEffect, useState } from "react";

const TitleEditer = () => {

    const [tabName, setTabName] = useState();
    useEffect(() => {
        document.title = tabName
    }, [tabName])

    return (
        <input
            type="text"
            placeholder="tabname"
            onChange={(evenement) => setTabName(evenement.target.value)}
        />
    );
}

export default TitleEditer;