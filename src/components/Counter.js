import React, { useState } from "react";

// Set the initial count state to zero, 0

const Counter =  () => {
  //
  const [count, setCount] = useState(42);
  return (
    <div>
      <h3>{count}</h3>
      <button onClick={()=>setCount(count+1)}>+</button>
      <button onClick={()=>setCount(count-1)}>-</button>
    </div>
  )
}

export default Counter;