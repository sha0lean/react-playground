import './App.css';
import Counter from './components/Counter';
import TitleEditer from './components/TitleEditer';

function App() {
  return (
    <div className="App">
      <TitleEditer />
      <Counter />
    </div>
  );
}

export default App;
